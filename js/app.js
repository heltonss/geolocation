'use strict';

var appGeo = angular.module('appGeo', ['ngRoute', 'appGeoControllers']);

appGeo.config(['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        $routeProvider.when('/', {
            templateUrl: 'partials/main.html',
            controller: 'MainCtrl'

        });

        $locationProvider.html5Mode(false).hashPrefix('!');
    }]);